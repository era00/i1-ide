const AdminBro = require('admin-bro');
const { Customer } = require('./customer.entity');

/** @type {AdminBro.ResourceOptions} */
const options = {};

module.exports = {
    options,
    resource: Customer,
};
