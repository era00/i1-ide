const mongoose = require('mongoose');

const CustomerEntity = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
    },
    address: {
        type: String,
        required: true,
    },
});

const Customer = mongoose.model('Customer', CustomerEntity);

module.exports = { CustomerEntity, Customer };
