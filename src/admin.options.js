const { default: AdminBro } = require('admin-bro');
const AdminBroMongoose = require('admin-bro-mongoose');

AdminBro.registerAdapter(AdminBroMongoose);

const AdminUser = require('./users/user.admin');
const { Customer } = require('./customers/customer.entity');

/** @type {import('admin-bro').AdminBroOptions} */
const options = {
    resources: [Customer],
    // resources: [AdminUser],
};

module.exports = options;
