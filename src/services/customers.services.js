const customerToBeImported = require('../../save/customers.json');
const { Customer } = require('../customers/customer.entity');

function importCustomer() {
    try {
        for (const customer of customerToBeImported) {
            Customer.create(customer);
        }
        return true;
    } catch (error) {
        console.log(error);
        return false;
    }
}

module.exports = {
    importCustomer,
};
