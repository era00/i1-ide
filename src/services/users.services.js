const customerToBeImported = require('../../save/customers.json');
const { User } = require('../users/user.entity');

function importUser() {
    User.create({
        userName: 'test',
        email: 'testTest',
        encryptedPassword:
            '$argon2i$v=19$m=4096,t=3,p=1$NT2qCXlKP2jEVdcJ3Nx0nA$pEOjFl825MMUxA0UYUkKSiC9aEHmDXCOJ0kED+Oc+o0',
    });
}

module.exports = {
    importUser,
};
